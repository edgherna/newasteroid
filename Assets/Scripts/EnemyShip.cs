﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShip : MonoBehaviour {

    public Vector3 direction;
	
    // Use this for initialization
	void Start ()
    {
        
	}
	
	// Update is called once per frame
	void Update ()
    {
        direction = new Vector3(0, 1, 0);
        transform.Translate(direction * Time.deltaTime * GameManager.instance.enemyShipSpeed);
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Board")
        {
            Destroy(this.gameObject);
        }
    }
}
