﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager instance;

    //Enemy Ship Spawn List
    public GameObject[] enemyShipSpawnPoints;

    //For the Enemy Ship prefabs
    public float enemyShipSpeed;
    public float enemyShipRotationSpeed;
    public List<GameObject> enemyShips;

    //For the bullet prefabs
    public GameObject bullet;
    public float laserSpeed;
    public float laserLife;

    //List of enemies
    public List<GameObject> activeEnemies;
    public bool removingEnemies;
    public int maximumNumberActiveEnemies;

    public GameObject player;
    public int numLives;
    
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start ()
    {
        activeEnemies = new List<GameObject>();
        removingEnemies = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        /*
        if ()
        {
            AddEnemyShip();
        }
        
        if ()
        {
            RemoveEnemyShip();
        }
        */
	}

    void AddEnemyShip()
    {
        if (activeEnemies.Count < maximumNumberActiveEnemies)
        {
            //Decide the Spawn Point
            int id = Random.Range(-1, 2);
            GameObject point = enemyShipSpawnPoints[id];

            //Initialize enemyShip
            GameObject enemyShip = new GameObject();

            //Instantiate the enemy ships
            GameObject enemyShipInstance = Instantiate<GameObject>(enemyShip, point.transform.position, Quaternion.identity);
            Vector2 directionVector = new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
            directionVector.Normalize();

            if (enemyShipInstance.GetComponent<EnemyShip>() != null)
            {
                enemyShipInstance.GetComponent<EnemyShip>().direction = directionVector;
            }

            //Add enemiesShips to list
            activeEnemies.Add(enemyShipInstance);
        }
    }
}
